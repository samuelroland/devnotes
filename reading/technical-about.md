# Technical about

*Let me explain how this web app works...*

All the content in written in Markdown and files are stored on Codeberg on [samuelroland/devnotes](https://codeberg.org/samuelroland/devnotes). I've designed a few Markdown extensions in PHP to design these pages.

The web app that displays this documentation is called X, it pulls different repositories, manage visibility for pages in private repositories (for internal documentations).

This application is designed with the delightful TailwindCSS framework.

Webhooks are configured to be pushed on /x and it will pull new commits and rebuild the HTML content and then cache it.