# Notes on the Larabit exercise about Pest on Laracasts
Video: [From PHPUnit to Pest](https://laracasts.com/series/andres-larabits/episodes/3)

## Advantages
- Distraction free
- Easier to read
- Maintained by Nuno Maduro

## Exercice
Write the code written in the video, this consists of transforming PHPUnit tests into Pest tests, one after one.

Quick setup of the Blog website example
```
git clone https://github.com/laracasts/lc-pest-example
git checkout -b base 691b1eab58850c57f967c0a947dc39c9d68795bf
mv lc-pest-example pestblog
cd pestblog
valet link
laravelquicksetup
codium .
```

## Things to remember:
### Setup of Pest
1. From the [Installation documentation](https://pestphp.com/docs/installation):

	```bash
	composer require pestphp/pest --dev --with-all-dependencies
	```

1. For Laravel only:

	todo:why?

	```
	composer require pestphp/pest-plugin-laravel --dev
	php artisan pest:install
	```

	It creates `Pest.php` that contains abstractions to avoid to have this code in our tests (like imports or class definitions)
	```php
	<?php

	uses(Tests\TestCase::class)->in('Feature');

	expect()->extend('toBeOne', function () {
		return $this->toBe(1);
	});

	```
	

1. Running tests
```
./vendor/bin/pest
# or 
pest
```
There is an extension called `Better Pest` on VSCodium to run tests, see chapter in [[pest]].

## Creating tests
We are using a subfolder `Pest` here to avoid mixing existing PHPUnit tests and new Pest tests...
```
php artisan pest:test Pest/ReadTest
```
This will create `tests/Feature/Pest/ReadTest.php`:
```php
<?php

it('has pest/read page', function () {
    $response = $this->get('/pest/read');

    $response->assertStatus(200);
});

```

## Refactor a first PHPUnit test
As we have no class, we can't import traits like `RefreshDatabase` so we have to say in the `Pest.php` that we want to use these classes...

We just need to add a `uses()` and import the class.
```php
<?php

use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class);

it('has pest/read page', function () {
	$response = $this->get('/pest/read');

	$response->assertStatus(200);
});

```

But we can move the 2 first lines in `Pest.php`:
```php

use Illuminate\Foundation\Testing\RefreshDatabase;

uses(RefreshDatabase::class)->in('Feature'); //use this classe in all tests in the Feature folder
```

## Create a unit test
```
php artisan pest:test MathCustomTest --unit
```

## Data providers ?
The equivalent is called datasets. 

```php
test("additions works with datasets", function ($num1, $num2, $expected) {
	$result = $this->math->add($num1, $num2);
	$this->assertEquals($expected, $result);
})->with([
	[1, 1, 2],
	[1, 6, 7],
	[1, 2, 3],
]);

```

## Running things before tests
Instead of `setUp` we need to use `beforeEach()`:

```php
beforeEach(function () {
	$this->math = new MathCustom;
});
```

## Expectations
Expections are another way to write assertions, with a more logic reading flow.

```php
$this->assertEquals(2, $result);
//Is equivalent to:
expect($result)->toBe(2);
```
